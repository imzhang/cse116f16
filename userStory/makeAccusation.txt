makeAccusation.txt
* Includes button, menu item, or other aspect of the GUI to select "Make Accusation"
	In the GUI, we used the keyevent to implement the function of the accusation.
	To make an accusation, press �A� on the keyboard to make an accusation.
* When "Make Accusation" selected, provides window to enter accusation
	When �A� is pressed, all the cards allowed will pop up and the player will be 
	able to select one card from each category (room, weapon, character).
	This is shown in the class Accusation.java in the GUI package.
* Window in which accusation entered includes way to specify the person, weapon, & room
	Player can only select one person, weapon and room from each category. The game 
	will take the last selected card from each category, so that player will not be 
	able to choose more than one card from each category. There are 3 Listeners; ACListener, AWListener, 
	and ARListener which implements the ActionListener. (Located in the package Listeners) Therefore, 
	everytime an image is clicked, the Listener will automatically specify which category.
* Does not allow user to enter person, weapon, & room that is not part of game OR shows error when invalid 
  person, weapon, or room entered
	Player is only allowed to select from the cards that pop up after pressing �A�. 
	The cards that appear should be all the cards of the game. When player starts the game, 
	all the cards available for accusation is already set so therefore player will not be able to enter
	other cards that are not relevant to the game. 
* Window in which accusation entered includes button for user to select once accusation entered
	Player will only be allowed to enter an accusation once. If they are wrong, 
	then they will not be able to press the button �A� to make another accusation. 
