package GUI;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import model.Board;

public class SecretPassage {
	
	private JLabel label;
	private Board _board;
	private int _num;
	public SecretPassage(Board board, int num){
		
		_board = board;
		_num = num; 
		int block = 30;
		label = new JLabel();
		
		ImageIcon pic = new ImageIcon(getClass().getResource("/images/secretpassage-min.png"));
		Image img = pic.getImage();
		Image newImg = img.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
		label.setIcon(new ImageIcon(newImg));
		label.setBounds(_board.spPosition.get(num)[1]*(block+11)-5, (_board.spPosition.get(num)[0]*(block+5)-10), 40, 40);
		
		
	}
	public JLabel getPassage(){
		return label;
	}
}
