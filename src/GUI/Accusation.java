package GUI;

import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import Listeners.ACListener;
import Listeners.ARListener;
import Listeners.AWListener;
import Listeners.CListener;
import Listeners.WListener;
import model.Card;
import model.Cards;
import model.Player;

public class Accusation {
	ArrayList<JButton> weapons;
	ArrayList<JButton> chars;
	ArrayList<JButton> rooms;
	
	String[] accusation = {"", "", ""};

	public Accusation(int block, GUI gui, Player p, Cards cards){

	
		rooms = new ArrayList<JButton>();
		String[] roomNames ={"Study", "Library","DiningRoom","Hall","Kitchen","Lounge","BallRoom","BillardRoom","Conservatory"};
		
		for(int i=0; i<roomNames.length; ++i){
			
				JButton b = new JButton();
				ImageIcon pic = new ImageIcon(getClass().getResource("/images/"+ roomNames[i] +"-min.png"));
				Image img = pic.getImage();
				Image newImg = img.getScaledInstance(100, block*5, java.awt.Image.SCALE_SMOOTH);
				b.setIcon(new ImageIcon(newImg));
				b.setBounds(i*100, 50, 100, block*5);	
				ARListener wl = new ARListener(accusation, roomNames, i, gui,cards);
				b.addActionListener(wl);
				rooms.add(b);
			
			

		}
		weapons = new ArrayList<JButton>();
		String[] weaponNames ={"Wrench", "Candlestick","Revolver","Knife","LeadPipe","Rope"};
		
		for(int i=0; i<weaponNames.length; ++i){
			
				JButton b = new JButton();
				ImageIcon pic = new ImageIcon(getClass().getResource("/images/"+ weaponNames[i] +"-min.png"));
				Image img = pic.getImage();
				Image newImg = img.getScaledInstance(140, block*5, java.awt.Image.SCALE_SMOOTH);
				b.setIcon(new ImageIcon(newImg));
				b.setBounds(i*150, 250, 140, block*5);	
				AWListener wl = new AWListener(accusation, weaponNames, i, gui,cards);
				b.addActionListener(wl);
				weapons.add(b);
			
			

		}
		

		chars = new ArrayList<JButton>();
		String[] charNames ={"MissScarlet", "MrGreen","MrsPeacock","MrsWhite","ProfPlum","ColonelMustard"};
		for(int i=0; i<charNames.length; ++i){
			
				JButton b = new JButton();
				ImageIcon pic = new ImageIcon(getClass().getResource("/images/"+ charNames[i] +"-min.png"));
				Image img = pic.getImage();
				Image newImg = img.getScaledInstance(140, block*5, java.awt.Image.SCALE_SMOOTH);
				b.setIcon(new ImageIcon(newImg));
				b.setBounds(i*150, 490, 140, block*5);	
				ACListener cl = new ACListener(accusation, charNames, i, gui,cards);
				b.addActionListener(cl);
				chars.add(b);
			
			

		}

	}
	public ArrayList<JButton> getRooms(){
		return rooms;
	}
	public ArrayList<JButton> getWapen(){
		return weapons;
	}
	public ArrayList<JButton> getChars(){
		return chars;
	}
	public String[] getAccu(){
		return accusation;
	}

}
