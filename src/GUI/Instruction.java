package GUI;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Instruction {
	private JLabel label;
	public Instruction(){
		label = new JLabel();
		ImageIcon pic = new ImageIcon(getClass().getResource("/images/keys-min.png"));
		Image img = pic.getImage();
		Image newImg = img.getScaledInstance(540, 320, java.awt.Image.SCALE_SMOOTH);
		label.setIcon(new ImageIcon(newImg));
		label.setBounds(900, 440, 540, 320);	
	}	
	public JLabel getInstruction(){
		return label;
	}
}
