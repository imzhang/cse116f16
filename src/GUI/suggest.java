package GUI;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import Listeners.CListener;
import Listeners.WListener;
import model.Card;
import model.Player;

//make suggestion option buttons
public class suggest {
	ArrayList<JButton> weapons;
	ArrayList<JButton> chars;
	String[] suggest = {"", "", ""};

	public suggest(int block, GUI gui, Player p){

	
		//make weapons buttons
		weapons = new ArrayList<JButton>();
		String[] weaponNames ={"Wrench", "Candlestick","Revolver","Knife","LeadPipe","Rope"};
		
		for(int i=0; i<weaponNames.length; ++i){
			if(!isContained(weaponNames[i], p.getMyCards())){
				JButton b = new JButton();
				ImageIcon pic = new ImageIcon(getClass().getResource("/images/"+ weaponNames[i] +"-min.png"));
				Image img = pic.getImage();
				Image newImg = img.getScaledInstance(140, block*5, java.awt.Image.SCALE_SMOOTH);
				b.setIcon(new ImageIcon(newImg));
				b.setBounds(i*150, 80, 140, block*5);	
				WListener wl = new WListener(suggest, weaponNames, i, gui);
				b.addActionListener(wl);
				weapons.add(b);
			}
			

		}
		
		//////////////

		chars = new ArrayList<JButton>();
		String[] charNames ={"MissScarlet", "MrGreen","MrsPeacock","MrsWhite","ProfPlum","ColonelMustard"};
		for(int i=0; i<charNames.length; ++i){
			if(!isContained(charNames[i], p.getMyCards())){
				JButton b = new JButton();
				ImageIcon pic = new ImageIcon(getClass().getResource("/images/"+ charNames[i] +"-min.png"));
				Image img = pic.getImage();
				Image newImg = img.getScaledInstance(140, block*5, java.awt.Image.SCALE_SMOOTH);
				b.setIcon(new ImageIcon(newImg));
				b.setBounds(i*150, 320, 140, block*5);	
				CListener cl = new CListener(suggest, charNames, i, gui);
				b.addActionListener(cl);
				chars.add(b);
			}
			

		}

	}
	private boolean isContained(String s, ArrayList<Card> al){
		for(int i=0; i<al.size(); ++i)
			if(al.get(i).getString().equals(s))
				return true;
		return false;
	}
	
	public ArrayList<JButton> getWapen(){
		return weapons;
	}
	public ArrayList<JButton> getChars(){
		return chars;
	}
	public String[] getSugg(){
		return suggest;
	}

}
