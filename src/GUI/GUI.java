package GUI;

import java.awt.Component;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;


import model.Board;
import model.Card;
import model.Cards;
import model.Player;
import model.func;

public class GUI extends JFrame{
	
	private JLayeredPane lp; 
	private Board _board;
	public int block;
	private Cards cards;
	public Turn turn;
	private Player[] players;
	private Token[] tokens;
	private Door[] doors;
	private SecretPassage[] passages;
	private int _steps;
	private Player suggester;
	private Player accuser;
	suggest s;
	Accusation a;
		
	public GUI(){
		
		super("Clue");
		block = 30;
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setBounds(0, 0, 80*block, 80*block);
		lp = this.getLayeredPane();
		
		_board = new Board(6);
		cards = new Cards();
		players = new Player[6];
		tokens = new Token[6];
		doors = new Door[16];
		passages = new SecretPassage[4];
		_steps = 0;
		
		
		this.setBackground();
		this.setInstruction();
		this.addingDoors();
		this.creatingPlayers();
		this.addingTokens();
		this.addingDice();
		this.addingScreen();
		
		cards.getEachPile6(players[0],players[1],players[2],players[3],players[4],players[5]);
		
		turn = new Turn(players[0],players[1],players[2],players[3],players[4],players[5]);
		this.addingTScreen(turn.getCurrentPlayer().getID());
		this.addingCards();
		this.addingSecretPassage();
		
		/////////
		//listeners
		//////////
		this.addKeyListener(new KeyListener(){

			@Override
			public void keyPressed(KeyEvent e) {
				
				if(_steps!=0 && e.getKeyCode()==KeyEvent.VK_UP){
				
					Player a = turn.getCurrentPlayer();
					a.move('w');
					updateToken();
//					if(a.movable(a.getTheRow()-1, a.getTheColumn())||_steps==1){
					updateSteps();
//					}
					updateScreen();
					gameloop();
				}
				if(_steps!=0 &&e.getKeyCode()==KeyEvent.VK_LEFT){
					
					Player a = turn.getCurrentPlayer();
					a.move('a');
					updateToken();
//					if(a.movable(a.getTheRow()+1, a.getTheColumn())||_steps==1){
					updateSteps();
//					}
					updateScreen();
					gameloop();
				}
				if(_steps!=0 &&e.getKeyCode()==KeyEvent.VK_DOWN){
					
					Player a = turn.getCurrentPlayer();
					a.move('s');
					updateToken();
//					if(a.movable(a.getTheRow(), a.getTheColumn()-1)||_steps==1){
					updateSteps();
//					}
					updateScreen();
					gameloop();
				}
				if(_steps!=0 &&e.getKeyCode()==KeyEvent.VK_RIGHT){
					
					Player a = turn.getCurrentPlayer();
					a.move('d');
					updateToken();
//					if(a.movable(a.getTheRow(), a.getTheColumn()+1)||_steps==1){
					updateSteps();
//					}
					updateScreen();
					gameloop();
				}
				if(_steps==0 &&e.getKeyCode()==KeyEvent.VK_Z){
					if(turn.getCurrentPlayer().getSID()==1){
						System.out.println(""+turn.getCurrentPlayer().getSID());
						turn.next();
						
					}else{
					_steps = turn.getCurrentPlayer().rollTheDice();
					updateScreen();
					updateNext();
					updateCards();
					}
					
				}
				if( _steps> 0 && e.getKeyCode()==KeyEvent.VK_ENTER){
				
					if(_board.enter(turn.getCurrentPlayer(),players)){
					_steps=0;
					}
					updateToken();
					updateScreen();
									}
				
				if( _steps> 0 && e.getKeyCode()==KeyEvent.VK_X){
					
					turn.getCurrentPlayer().exit();
					_steps = _steps - 1;
					updateScreen();
					updateToken();
				
					
				}
				if(_steps> 0 && e.getKeyCode()==KeyEvent.VK_C){
					if(_board.useSecretPassage(turn.getCurrentPlayer())){
					_steps=0;
					}
					updateToken();
					updateScreen();
				}
				
				int pos = turn.getCurrentPlayer().getPosition();
				if( (pos>=10&&pos<=20) &&e.getKeyCode()==KeyEvent.VK_S){
			
					updateTran();
					updateSuggest1();

					
				}
				if(e.getKeyCode()==KeyEvent.VK_A){
					
					updateTran();
					updateAccusation1();

					
				}
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			
			
		});

		for(int i=0; i<3; ++i)
			System.out.println(cards.getAnswer().get(i).getString());
		
	
	}
	
	public void gameloop(){
		if(_steps == 0){
			turn.next();
		}
	}
	
	public void creatingPlayers(){
		for(int i=0;i<6;i++){
			players[i] = new Player(_board,i,cards);
		}
	}
	
	public void setBackground(){
		Background back = new Background();
		lp.add(back.getBackground(), new Integer(0));
	}
	
	public void setInstruction(){
		Instruction in = new Instruction();
		lp.add(in.getInstruction(), new Integer(1));
	}
	
	public void addingTokens(){
	
		for(int i=1;i<7;i++){
			tokens[i-1] = new Token(_board,i,players[i-1]);
			lp.add(tokens[i-1].getToken(), new Integer(i+45));
		}
	
	}
	
	public void addingDoors(){
		
		for(int i=0;i<15;i++){
			doors[i] = new Door(_board,i);	
			lp.add(doors[i].getDoor(), new Integer(i+5));
		}
		
		
	}
	public void addingSecretPassage(){
		for(int i=0;i<4;i++){
			passages[i] = new SecretPassage(_board,i);
			lp.add(passages[i].getPassage(), new Integer(i+25));
		}
	}
	
	public void addingDice(){
		DiceButton button = new DiceButton();
		lp.add(button.getButton(),new Integer(4));
	}
	
	public void addingScreen(){
		NumDisplay nd = new NumDisplay(_steps);
		lp.add(nd.getScreen(), new Integer(3));
	}
	
	public void addingTScreen(int num){
		
		num = turn.getCurrentPlayer().getID()+1;
		JLabel TScreen = new JLabel();
		ImageIcon pic = new ImageIcon(getClass().getResource("/images/p"+num+"-min.png"));
		Image img = pic.getImage();
		Image newImg = img.getScaledInstance(280, 190, java.awt.Image.SCALE_SMOOTH);
		TScreen.setIcon(new ImageIcon(newImg));
		TScreen.setBounds(1165, 2, block*30, block*5);	
		lp.add(TScreen, new Integer(2));
		
	}
	
	public void addingCards(){
		
		GCard[] cards = new GCard[3];
		for(int i=0;i<3;i++){
		cards[i] = new GCard(turn.getCurrentPlayer(),i);
		lp.add(cards[i].getCard(), new Integer(i+35));
		}
		
	}
	
	public void updateSuggestions(){
		this.removeLayer(2);
		this.addingTScreen(turn.getCurrentPlayer().getID());
	}
	public void updateScreen(){
		
		this.removeLayer(3);
		NumDisplay nd = new NumDisplay(_steps);
		lp.add(nd.getScreen(), new Integer(3));
		
	}
	
	
	public void updateNext(){
		this.removeLayer(2);
		addingTScreen(turn.getCurrentPlayer().getID());
	}
	
	public void updateToken(){
		
		for(int i=1;i<7;i++){
			this.removeLayer(i+45);
		}
		for(int i=1;i<7;i++){
			tokens[i-1] = new Token(_board,i,players[i-1]);
		}
		for(int i=1;i<7;i++){
			lp.add(tokens[i-1].getToken(), new Integer(i+45));
		}
		
		this.repaint();
	}
	
	public void updateCards(){
		for(int i =0;i<3;i++){
			this.removeLayer(i+35);
		}
		this.addingCards();
	}
	
	public void updateSteps(){
		_steps = turn.getCurrentPlayer()._steps;
	}
	
	//add transparent layer (for suggestion and rejection)
	public void updateTran(){
		Trans t = new Trans(block);
		lp.add(t.getTrans(), new Integer(55));
	}
	
	//put suggestion option buttons on
	public void updateSuggest1(){

		s = new suggest(block, this, turn.getCurrentPlayer());
		suggester = turn.getCurrentPlayer();
		ArrayList<JButton> sw =s.getWapen();
		for(int i=0; i<sw.size(); ++i)
			lp.add(sw.get(i), new Integer(56));
		ArrayList<JButton> sc =s.getChars();
		for(int i=0; i<sc.size(); ++i)
			lp.add(sc.get(i), new Integer(56));

	}
	
	//push suggestion to data structure
	public void updateSuggest2(){
		String[] sug = s.getSugg();
		Player p = turn.getCurrentPlayer();
		p.makeASuggestion(new Card(sug[1]), new Card(func.num2room(p.getPosition())+""), new Card(sug[0]));
		this.updateReject();
	}
	
	public void updateAccusation1(){
		a = new Accusation(block, this, turn.getCurrentPlayer(),cards);
		accuser = turn.getCurrentPlayer();
		ArrayList<JButton> ar =a.getRooms();
		for(int i=0; i<ar.size(); ++i)
			lp.add(ar.get(i), new Integer(57));
		ArrayList<JButton> aw =a.getWapen();
		for(int i=0; i<aw.size(); ++i)
			lp.add(aw.get(i), new Integer(57));
		ArrayList<JButton> ac =a.getChars();
		for(int i=0; i<ac.size(); ++i)
			lp.add(ac.get(i), new Integer(57));
	}
	
	public void updateAccusation2(){
		String[] accu = a.getAccu();
		Player p = turn.getCurrentPlayer();
		p.makeAnAccusation(new Card(accu[1]), new Card(accu[2]), new Card(accu[0]));
		
	}
	
	//add rejection panel for other players to reject
	public void updateReject(){
		this.removeLayer(56);
		this.gameloop();
		//show rejecter's cards
		Reject rj = new Reject(turn.getCurrentPlayer(), this, block, cards, suggester.getID(), turn.getCurrentPlayer().getID());
		JButton[] r = rj.getCard();
		for(int i=0; i<r.length; ++i)
			lp.add(r[i], new Integer(56));
		lp.add(rj.getTitle(), new Integer(56));
		
		//show current suggestion
		Suggested sg = new Suggested(cards, block);
		JLabel[] s = sg.getSugg();
		for(int i=0; i<3; ++i)
			lp.add(s[i], new Integer(56));
		
		this.repaint();
	}

	//end suggestion stage, back to moving stage
	public void endSuggest(){
		cards.clearSuggestion();
		this.removeLayer(55);
		this.removeLayer(56);
		turn.setCurrentPlayer(suggester);
		turn.next();
		updateScreen();
		updateNext();
		updateCards();
		this.requestFocus();
		this.repaint();
	}
	
	public void endAccu(){
		cards.clearAccusation();
		this.removeLayer(55);
		this.removeLayer(57);
		turn.getCurrentPlayer().isFail();
		turn.getCurrentPlayer()._steps=0;
		updateSteps();
		updateScreen();
		this.requestFocus();
		this.repaint();
	}
	
	//add  restart (game-over) button
	public void restartButton(){
		Restart r = new Restart(block, this);
		lp.add(r.getButton(), new Integer(58));
	}
	
	//When restart button pushed, close game window
	public void restart(){
		this.dispose();
	}
	
	public void removeLayer(int i){
		Component[] com = lp.getComponentsInLayer(i);
		for(int j=0; j<com.length; j++){
			lp.remove(com[j]);
		}
	}
	
}
