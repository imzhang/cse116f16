package GUI;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;


public class DiceButton {
	private JLabel label;
	private int block;
	
	public DiceButton(){
		
		label = new JLabel();
		block = 30;
		ImageIcon pic = new ImageIcon(getClass().getResource("/images/dice-min.png"));
		Image img = pic.getImage();
		Image newImg = img.getScaledInstance(block*5, block*5, java.awt.Image.SCALE_SMOOTH);
		label.setIcon(new ImageIcon(newImg));
		label.setBounds(895, 2, block*5, block*5);	
		
	}
	public JLabel getButton(){
		return label;
	}
}
