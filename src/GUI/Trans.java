package GUI;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
//make transparent layer
public class Trans {
	private JLabel screen;
	
	public Trans(int block){
		screen = new JLabel();
		ImageIcon pic = new ImageIcon(getClass().getResource("/images/transparent-min.png"));
		Image img = pic.getImage();
		Image newImg = img.getScaledInstance(block*30, block*25, java.awt.Image.SCALE_SMOOTH);
		screen.setIcon(new ImageIcon(newImg));
		screen.setBounds(0, 0, block*30, block*25);	
	}
	public JLabel getTrans(){
		return screen;
	}
}
