package GUI;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import Listeners.NextListener;
import model.Cards;
import model.Player;

//make rejection option buttons, NEXT button
public class Reject {
	
	private JButton[] card;
	private JLabel title;
	
	public Reject(Player player, GUI gui, int block, Cards cards, int suggerster, int cur){
		int n = player.checkSuggestion().size();
		System.out.println("player card n: "+n);
		
		card = new JButton[n+1];
		for(int i=0; i<n; ++i){
			card[i] = new JButton();
			String s = new String();
			s= player.checkSuggestion().get(i).getString();
			ImageIcon pic = new ImageIcon(getClass().getResource("/images/"+ s +"-min.png"));
			Image img = pic.getImage();
			Image newImg = img.getScaledInstance(140, block*5, java.awt.Image.SCALE_SMOOTH);
			card[i].setIcon(new ImageIcon(newImg));
			card[i].setBounds((890/2)-100, 400 + i*block*5, 140, block*5);	
			card[i].addActionListener(new ActionListener(){

				@Override
				public void actionPerformed(ActionEvent arg0) {
					gui.endSuggest();
					
				}
				
			});
			
		}
		card[n] = new JButton("NEXT");
		card[n].setFont(new Font("Courier", Font.BOLD, 44));
		card[n].setBounds(890, 450 , 550, 310);
		NextListener nl = new NextListener(cards,gui, block, suggerster, cur);
		card[n].addActionListener(nl);
		
		title = new JLabel("It is Player: "+ player.getID()+"'s Reject Turn");
		title.setBounds(0,0, block, 100*block);
	}
	public JButton[] getCard(){
		return card;
	}
	
	public JLabel getTitle(){
		return title;
	}
}
