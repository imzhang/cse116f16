package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

//add restart button
public class Restart {
	JButton b;
	GUI gui;
	
	public Restart(int block, GUI gui){
		b = new JButton("You win!");
		b.setBounds(345, 10*block+100, 5*block, 5*block);

		b.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				gui.restart();
			}
			
		});
	}
	public JButton getButton(){
		return b;
	}

}

