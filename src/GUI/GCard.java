package GUI;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import model.Player;

public class GCard {
	
	private JLabel label;
	private Player _player;
	
	public GCard(Player player, int num){
		label = new JLabel();
		_player = player;
		int block = 30;
		String s = new String();
		s= _player.getMyCards().get(num).getString();
		ImageIcon pic = new ImageIcon(getClass().getResource("/images/"+ s +"-min.png"));
		Image img = pic.getImage();
		Image newImg = img.getScaledInstance(181, 300, java.awt.Image.SCALE_SMOOTH);
		label.setIcon(new ImageIcon(newImg));
		label.setBounds(898 + (num*block*6), -75, block*10, block*25);	
		
	}
	public JLabel getCard(){
		return label;
	}
}
