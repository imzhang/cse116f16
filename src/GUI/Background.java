package GUI;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Background {
	
	JLabel back;
	
	public Background() {
	   
		  
		  	back = new JLabel();
			ImageIcon pic = new ImageIcon(getClass().getResource("/images/boardpan-min.png"));
			Image img = pic.getImage();
			Image newImg = img.getScaledInstance(920, 780, java.awt.Image.SCALE_SMOOTH);
			back.setIcon(new ImageIcon(newImg));
			back.setBounds(-15, -10, 920, 780);	
			
	  }
	
	public JLabel getBackground(){
		return back;
	}
	
}

