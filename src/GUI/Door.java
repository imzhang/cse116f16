package GUI;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import model.Board;

public class Door {
	
	private JLabel label;
	private int _num;
	private Board _board;
	public Door(Board board,int num){
		
		_board = board;
		_num = num;
		int block = 30;
		label = new JLabel();
		
		ImageIcon pic = new ImageIcon(getClass().getResource("/images/door-min.png"));
		Image img = pic.getImage();
		Image newImg = img.getScaledInstance(40, 40, java.awt.Image.SCALE_SMOOTH);
		label.setIcon(new ImageIcon(newImg));
		label.setBounds(_board.doorsPosition.get(num)[1]*(block+11)-5, (_board.doorsPosition.get(num)[0]*(block+5)-10), 40, 40);
	}
	
	public JLabel getDoor(){
		return label;
	}
	
}
