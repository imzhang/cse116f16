package GUI;


import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import model.Board;
import model.Cards;
import model.Player;

public class Token{
	
	private JLabel label;
	private Board _board;
	private int _num;
	private Player _player;
	public int block;
	
	public Token(Board board,int num,Player player){
		
		_num = num;
		_board = board;
		label = new JLabel();
		Cards cards = new Cards();
		block = 30;
		_player = player;
		
		ImageIcon pic = new ImageIcon(getClass().getResource("/images/"+ num +"-min.png"));
		Image img = pic.getImage();
		Image newImg = img.getScaledInstance(25, 25, java.awt.Image.SCALE_SMOOTH);
		label.setIcon(new ImageIcon(newImg));
		label.setBounds(_board.playerPosition.get(_num-1)[1]*(block+12)-5,_board.playerPosition.get(_num-1)[0]*(block+5),70,23);
		
	}
	
	public JLabel getToken(){
		return label;
	}
	
}
