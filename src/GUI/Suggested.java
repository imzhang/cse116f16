package GUI;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import model.Cards;
import model.Player;
import model.func;
//make label for showing current suggestion 
public class Suggested {
	private JLabel[] sugg;
	
	public Suggested(Cards card, int block){
		sugg = new JLabel[3];
		for(int i=0; i<3; ++i){
			sugg[i] = new JLabel();
			String s = new String();
			s= card.getSuggestion().get(i).getString();
			ImageIcon pic = new ImageIcon(getClass().getResource("/images/"+ s +"-min.png"));
			Image img = pic.getImage();
			Image newImg = img.getScaledInstance(140, block*5, java.awt.Image.SCALE_SMOOTH);
			sugg[i].setIcon(new ImageIcon(newImg));
			sugg[i].setBounds(280*i+65, 180, 140, block*5);	
		}
		
	}
	
	public JLabel[] getSugg(){
		return sugg;
	}
}
