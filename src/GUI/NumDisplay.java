package GUI;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class NumDisplay {
	private JLabel screen;
	private int _num;
	public NumDisplay(int num){
		
		_num = num;
		screen = new JLabel();
		int block = 30;
		ImageIcon pic = new ImageIcon(getClass().getResource("/images/d" + _num + "-min.png"));
		Image img = pic.getImage();
		Image newImg = img.getScaledInstance(block*5, block*5, java.awt.Image.SCALE_SMOOTH);
		screen.setIcon(new ImageIcon(newImg));
		screen.setBounds(1020, 0, block*10, block*5);	
		
	}
	
	public JLabel getScreen(){
		return screen;
	}
}
