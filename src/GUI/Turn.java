package GUI;

import model.Player;

	public class Turn {
		Player p1;
		Player p2;
		Player p3;
		Player p4;
		Player p5;
		Player p6;
		Player cur;


		public Turn(Player play1, Player play2, Player play3, Player play4, Player play5, Player play6 ){
			p1 = play1;
			p2 = play2;
			p3 = play3;
			p4 = play4;
			p5 = play5;
			p6 = play6;
			cur = p1;
		}
		
		public void next(){
			if(cur == p1)
				cur = p2;
			else if(cur == p2)
				cur = p3;
			else if(cur == p3)
				cur = p4;
			else if(cur == p4)
				cur = p5;
			else if(cur == p5)
				cur = p6;
			else if(cur == p6)
				cur = p1;
			
		}
		
		public Player getCurrentPlayer(){
			return cur;
		}
		public void setCurrentPlayer(Player p){
			cur = p;
		}
		
}
