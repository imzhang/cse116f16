package model; 

import java.util.ArrayList;


/**
 * Class for the players which is used represent a player in the game.
 * 
 * @author chenhaoxian
 * @author Isabel Zhang
 * @author Meiyu Chen
 */
public class Player{
/**
 * ArrayList of cards is created to hold all the cards that the player has.
 */
	private ArrayList<Card> _myCards;
/**
 * Dice is created to get the steps that the players should go
 */
	private Dice _dice;
/**
 * _position is the position of the player in the board in which position is named as
 * label. 
 */
	private int _position;
/**
 * _steps is the number that we get after rolling the dice
 */
	public int _steps;
/**
 * _board is created for convenience to share the position information publicly in the board
 */
	private Board _board;
/**
 * _id is the player's ID in the game, each player is labeled in a specific number
 */
	private int _id;
	
	private int _sid;
/**
 * prePosition is created to hold all the spaces that the player passed when moving.
 */
	private ArrayList<int[]> prePosition;
/**
 * _cards is created for the convenience of sharing the suggestion information of
 * the player.
 */
	private Cards _cards;
/**
 * _suggestion is created for hold the suggestion made by the player
 */
	private ArrayList<Card> _suggestion;
	
	public int[] original;
	
	ArrayList<Card> showOutCards;
	
/**
 * The constructor defines what the player should be, and their properties should include
 * their number, their coordinations in the board, the cards in their hands, how many steps
 * they should walk, their suggestions and their positions in the board.
 *  
 * @param b
 * @param id
 * @param card
 */
	public Player(Board b,int id, Cards card) {
	
	_id =id;
	_sid = 0;
	_board = b;
	_dice = new Dice();
	_myCards = new ArrayList<Card>();
	_cards = card;
	original = new int[2];
	
	_suggestion = new ArrayList<Card>();
	
	_position = 0;
	_steps = 0;
	
	prePosition = new ArrayList<int[]>();
	
	}

/**
 * This method is created to roll the dice and should finally return the 
 * number of steps that player could go
 * @return
 */
	public int rollTheDice() {
		_steps = _dice.shuffle();
		//System.out.println(_steps);
		return _steps;
	}
	
	public int getID(){
		return _id;
	}
	
	public int getSID(){
		return _sid;
	}
/**
 * This method returns the cards in the player's hand.
 * @return
 */
	public ArrayList<Card> getMyCards(){
		return _myCards;
	}
/**
 * This method is created to set what cards are in the player's hand.	
 * @param cards
 */
	public void setMyCards(ArrayList<Card> cards){
		_myCards = cards;
	}
/**
 * This method is created to set the coordination and the position of the player
 * in the board.
 * 	
 * @param row
 * @param column
 */
	public void setThePosition(int row, int column){
		_board.playerPosition.get(_id)[0] = row;
		_board.playerPosition.get(_id)[1] = column;
		_position = _board._space[row][column];
	}
/**
 * This method is created to set the position/label of the player in the game.	
 * @param label
 */
	public void setTheLabel(int label){
		_position = label;
	}
/**
 * This method is created to set how many steps that the player can walk.	
 * @param nums
 * @return
 */
	public int setTheSteps(int nums){
		_steps = nums;
		return _steps;
	}
/**
 * This method is created to get the value of X-coordination.
 * @return
 */
	public int getTheRow(){
		
	return _board.playerPosition.get(_id)[0];
	
	}
/**
 * This method is created to get the value of Y-coordination.
 * @return
 */
	public int getTheColumn(){
		
	return _board.playerPosition.get(_id)[1];
	}
/**
 * This method is created to get the position/label in the board.	
 * @return
 */
	public int getPosition(){
		return _position;
	}
	
/**
 * clear previous player's steps. In this way, we can record steps for next player.
 */
	public void clearPrePosition(){
			prePosition.clear();
	}
/**
 * This method is created to move the player. If the player inputs 'w', which means
 * going upwards and the Y-coordination will decrease one; if the player inputs 'a'
 * the player can go leftwards and reduce the Y-coordination by one; if the player inputs
 * 's', the player can go downwards and increase the X-coordination by one; if the player
 * inputs 'd', the player can go rightwards and increase the Y-coordination by one.
 * @param dir
 */
	public void move(char dir){
				if(dir=='w'){
					if(movable(getTheRow()-1, getTheColumn())&&_steps>0){
						this.setThePosition(getTheRow()-1, getTheColumn());
						//record positions that have been walked
						int[] a = {getTheRow(), getTheColumn()};
						prePosition.add(a);
						_steps--;
					}
				}
				if(dir=='s'){
					if(movable(getTheRow()+1, getTheColumn())&&_steps>0){
					this.setThePosition(getTheRow()+1, getTheColumn());
					
					int[] a = {getTheRow(), getTheColumn()};
					prePosition.add(a);
					_steps--;}
					
				}
				if(dir=='a'){
					if(movable(getTheRow(), getTheColumn()-1)&&_steps>0){
					 this.setThePosition(getTheRow(), getTheColumn()-1);
					
					int[] a = {getTheRow(), getTheColumn()};
					prePosition.add(a);
					_steps--;}

				}
				          
				
				if(dir=='d'){
					if(movable(getTheRow(), getTheColumn()+1)&&_steps>0){
				    this.setThePosition(getTheRow(), getTheColumn()+1);
					
					int[] a = {getTheRow(), getTheColumn()};
					prePosition.add(a);
					_steps--;}
			    }
				
				clearPrepo();
			}
/**
 * This method is created to check if the player moves correctly by restricting
 * their steps can not be larger than the value of the dice, they can only walk
 * on the hallway or pass the door, they can not move backwards.
 * @param row
 * @param col
 * @return
 */
		 public boolean movable(int row, int col){
			    if(_steps==0){
				 return false;
			    }
				//check if out of bounds
				if((row<0||row>22)||(col<0||col>22)){return false;}
				//check if there are rooms and hallways
				if(_board._space[row][col]!=0&&_board._space[row][col]<100){
					return false;
				}
				//check if there are players,if one player is in that position,then others cant walk there.
				for(int i=0;i<_board.playerPosition.size();i++){
					if(row==_board.playerPosition.get(i)[0]&&col==_board.playerPosition.get(i)[1]){
						return false;
					}
				}
			//make sure they cant walk backward
				for(int i=0; i<prePosition.size(); ++i){
					if(prePosition.get(i)[0] == row && prePosition.get(i)[1] == col)
						return false;
				}
				
				return true;
			}
/**
 * This method is created to set the value in the instance of suggestion.
 * When the player sets the suggestion, the suggestion variable should hold
 * the cards that the player suggested.
 * 	
 * @param character
 * @param room
 * @param weapon
 */
	public void makeASuggestion(Card character, Card room, Card weapon){
		
		if(getPosition()!=0&&getPosition()<100){
			/*
		_cards.getSuggestion().set(0, character);
		_cards.getSuggestion().set(1, weapon);
		_cards.getSuggestion().set(2, room);
		*/
			_cards.getSuggestion().clear();
			_cards.getSuggestion().add(character);
			_cards.getSuggestion().add(room);
			_cards.getSuggestion().add(weapon);
			
		}
		
	}
	public void makeAnAccusation(Card character, Card room, Card weapon){
		
		_cards.getAccusation().clear();
		_cards.getAccusation().add(character);
		_cards.getAccusation().add(room);
		_cards.getAccusation().add(weapon);
		
	}
/**
 * This method is created to check if the cards in the players' hands have
 * 	the cards that was suggested by other players.
 * @return
 */
	public ArrayList<Card> checkSuggestion(){
		showOutCards = new ArrayList<Card>();
		for(int i=0; i<_myCards.size(); ++i){
			String cur = _myCards.get(i).getString();
			for(int j=0; j<_cards.getSuggestion().size(); ++j)
				if(cur.equals(_cards.getSuggestion().get(j).getString()))
					showOutCards.add(_cards.getSuggestion().get(j));
		}
		return showOutCards;
	}
		
	
/**
 * This method is created to add one card to player.
 * 	
 * @param card
 */
	public void setCard(Card card){
		_myCards.add(0, card);
	}
/**
 * This method is created to clear the content inside the suggestion.	
 */
	public void clearSuggestion(){
		_suggestion.clear();
	}
	
	public void clearPrepo(){
		if(_steps==0){
			prePosition.clear();
		}
	}
	
	public void exit(){
		if(this.getPosition()>=10&&this.getPosition()<=20){
			this.setThePosition(original[0], original[1]);
			original[0] = 0;
			original[1] = 0;
			this._steps -=1;
		}
	}
	
	public int isFail(){
		_sid = 1;
		return _sid;
	}
	
}
