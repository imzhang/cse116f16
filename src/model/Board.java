package model;

import java.util.ArrayList;


/**
 * Class which implements the board of the game
 * which is based on an array whose scale is 22 by 22
 * 
 * @author chenhaoxian
 * @author Isabel Zhang
 * @author Meiyu Chen
 *
 */
public class Board{
/** Array which is based on the primitive type of int
 * This is used to represent the spaces in the board
 * of the game
 */
	public int[][] _space;
/**
 * ArrayList which is used to store each player's position
 * in the game
 */
	private int _numberOfPlayers;
/**
 * ArrayList of the card holding the suggestion of the players	
 */
	public ArrayList<Card> _suggestion;
/**
 * ArrayList of the int[] holding the position of the players	
 */
	public ArrayList<int[]> playerPosition;
	
	public ArrayList<int[]> doorsPosition;
	
	public ArrayList<int[]> spPosition;
/**
 * The instance variable _space is instantiated in the constructor
 * 
 * Each room is labeled in numbers from 11 to 19 in the array of the Board
 * 
 * Use the method addRooms2Board to assign each space in the board to each
 * space in the array of the room 
 *
 * Label the door of each room as the format of 1**, the first two digits 
 * is the label of the room, and the last one is number of the door of each
 * room
 * 
 * Label the secret passage of study room as 3, the secret passage of lounge as 4
 * the secret passage of conservatory as 5, the secret passage of kitchen as 6
 * 
 * Because the default value of int in Java is 0, so the Hallway is those
 * spaces which are not assigned to anything and those values are 0
 * 
 * @param numPlayer
 */
	public Board(int numPlayer){
		
		playerPosition = new ArrayList<int[]>();
		doorsPosition = new ArrayList<int[]>();
		spPosition = new ArrayList<int[]>();
		_numberOfPlayers = numPlayer;
		
		
		numPlayers();
		numDoors();
		numspPosition();
		
		_space = new int[22][22];	
		
		for(int i=0; i<22; ++i){
			for(int j =0; j<22; ++j){
				_space[j][i] = 0;
			}
		}
		
		//study
		for(int i=0; i<6; ++i){
			for(int j =0; j<3; ++j){
				_space[j][i] = 11;
			}
		}
		
		//hall
		for(int i=8; i<14; ++i){
			for(int j =0; j<6; ++j){
				_space[j][i] = 12;
			}
		}
//lounge
		for(int i=16; i<21; ++i){
			for(int j =0; j<5; ++j){
				_space[j][i] = 13;
			}
		}
		//library
		for(int i=0; i<5; ++i){
			for(int j =5; j<10; ++j){
				_space[j][i] = 14;
			}
		}
		//clue
		for(int i=8; i<13; ++i){
			for(int j =7; j<14; ++j){
				_space[j][i] = 15;
			}
		}
		//dinning room
		for(int i=15; i<22; ++i){
			for(int j =8; j<14; ++j){
				_space[j][i] = 16;
			}
		}
		//billiardRoom
		for(int i=0; i<5; ++i){
			for(int j =11; j<16; ++j){
				_space[j][i] = 17;
			}
		}
		//conse room
		
		for(int i=0; i<5; ++i){
			for(int j =18; j<22; ++j){
				_space[j][i] = 18;
			}
		}
		//Bau Room
		for(int i=7; i<15; ++i){
			for(int j =16; j<22; ++j){
				_space[j][i] = 19;
			}
		}
		//kitchen 
		for(int i=17; i<22; ++i){
			for(int j =17; j<22; ++j){
				_space[j][i] = 20;
			}
		}
		int doorOfStudy = 111;
		_space[3][5] = doorOfStudy;
		
		int doorOfHall1 = 121;
		int doorOfHall2 = 122;
		int doorOfHall3 = 123;
		_space[3][7] = doorOfHall1;
		_space[6][10] = doorOfHall2;
		_space[6][11] = doorOfHall3;
		
		int doorOfLounge = 131;
		_space[5][16] = doorOfLounge;
		
		int doorOfLibrary1 = 141;
		int doorOfLibrary2 = 142;
		_space[7][5] = doorOfLibrary1;
		_space[10][2] = doorOfLibrary2;
		
		int doorOfBilliard1 = 171;
		int doorOfBilliard2 = 172;
		_space[10][0] = doorOfBilliard1;
		_space[14][5] = doorOfBilliard2;
		
		int doorOfDining1 = 161;
		int doorOfDining2 = 162;
		_space[7][16] = doorOfDining1;
		_space[11][14] = doorOfDining2;
		
		int doorOfConservatory = 181;
		_space[18][5] = doorOfConservatory;
		
		int doorOfBathRoom1 = 191;
		int doorOfBathRoom2 = 192;
		int doorOfBathRoom3 = 193;
		int doorOfBathRoom4 = 194;
		_space[18][6] = doorOfBathRoom1;
		_space[15][8] = doorOfBathRoom2;
		_space[15][13] = doorOfBathRoom3;
		_space[18][15] = doorOfBathRoom4;
		
		int doorOfKicthen = 201;
		_space[16][18] = doorOfKicthen;
		
		int secretPassageOfStudy = 3;
		_space[2][0] = secretPassageOfStudy;
		int secretPassageOfLounge = 4;
		_space[4][21] = secretPassageOfLounge;
		int secretPassageOfConservatory = 5;
		_space[18][0] = secretPassageOfConservatory;
		int secretPassageOfKitchen = 6;
		_space[21][17] = secretPassageOfKitchen;
		
//		print();
	}
/**
 * This method is used to test the whole arrangement of all the elements in
 * the board
 * 	
 */
	public void print(){
		for(int i=0; i<22; ++i){
			for(int j = 0; j<22; ++j){
				System.out.print(_space[i][j]);
			}
			System.out.println();
		}
	}

	public void numPlayers(){
		
			int[] num1 = {4,0};
			int[] num2 = {6,21};
			int[] num3 = {0,15};
			int[] num4 = {17,0};
			int[] num5 = {21,6};
			int[] num6 = {21,15};
			ArrayList<int[]> nums = new ArrayList<int[]>();
			nums.add(num1);
			nums.add(num2);
			nums.add(num3);
			nums.add(num4);
			nums.add(num5);
			nums.add(num6);
			
			for(int i=0;i<_numberOfPlayers;i++){
				playerPosition.add(nums.get(i));
			}
		
	}
	public void numDoors(){
		
			int[] num1 = {3,5};
			int[] num2 = {3,7};
			int[] num3 = {6,10};
			int[] num4 = {6,11};
			int[] num5 = {5,16};
			int[] num6 = {7,5};
			int[] num8 = {10,0};
			int[] num9 = {14,5};
			int[] num10 = {7,16};
			int[] num11 = {11,14};
			int[] num12 = {18,5};
			int[] num13 = {18,6};
			int[] num14 = {15,13};
			int[] num15 = {18,15};
			int[] num16 = {16,18};
			
			ArrayList<int[]> nums = new ArrayList<int[]>();
			nums.add(num1);
			nums.add(num2);
			nums.add(num3);
			nums.add(num4);
			nums.add(num5);
			nums.add(num6);
			nums.add(num8);
			nums.add(num9);
			nums.add(num10);
			nums.add(num11);
			nums.add(num12);
			nums.add(num13);
			nums.add(num14);
			nums.add(num15);
			nums.add(num16);
			
			for(int i=0;i<15;i++){
				doorsPosition.add(nums.get(i));
			}
	}
	
	public void numspPosition(){
		
		int[] num1 = {2,0};
		int[] num2 = {4,21};
		int[] num3 = {18,0};
		int[] num4 = {21,17};
		
		ArrayList<int[]> nums = new ArrayList<int[]>();
		nums.add(num1);
		nums.add(num2);
		nums.add(num3);
		nums.add(num4);
		
		for(int i=0;i<4;i++){
			spPosition.add(nums.get(i));
		}
		
	}
/**
 * Use a two dimensional loop to go through each space in the room array
 * and then assign each space of the Board to each space on the Board
 * 
 * @param row
 * @param column
 * @param room
 */
	public void addRooms2Board(int row,int column,int[][] room){
		
		int left = row + room.length;
		int right = column + room[0].length;
		
		for(int i=0;i<room.length;i++){
			for(int j=0;j<room[0].length;j++){
				_space[row][column] = room[i][j];
				if(column < right){
					column ++;
				}else if(row < left){
					row ++;
					column = column - room[0].length;
				}
			}
		}
	}
/**
 * This method is used to set the position of the player when they choose to
 * use the secret passage to enter other rooms.
 * @param player
 * @return
 */
	public boolean useSecretPassage(Player player){
		int position;
		position = player.getPosition();
		if(position==11||position==13||position==17||position==19){
			int label = position / 3;
			if(label==3){
			player.setTheLabel(6);
			player.setThePosition(21, 17);
			}
			if(label==4){
			player.setTheLabel(5);
			player.setThePosition(18, 0);
			}
			if(label==5){
			player.setTheLabel(4);
			player.setThePosition(4, 21);
			}
			if(label==6){
			player.setTheLabel(3);
			player.setThePosition(2, 0);
			}
			return true;
		}
		return false;
	}
	
/**
 * This method is used to let the players enter the rooms. 
 * @param player
 * @return
 */
	public boolean enterARoom(Player player,Player[] players){
		int position;
		position = player.getPosition();
		int label;
		label = position/10;
//		for(Player i:players){
//			if(i.getPosition()==label){
//				return false;
//			}
//		}
		if(label>=10&&label<=20){
			player.setTheLabel(label);
			return true;
		}
		return false;
	}
	
	public boolean enter(Player player,Player[] players){
		if(enterARoom(player,players)==true){
			player.original[0] = player.getTheRow();
			player.original[1] = player.getTheColumn();
			if(player.getPosition()==11){
				player.setThePosition(1,3);
				return true;
			}
			if(player.getPosition()==12){
				player.setThePosition(3,10);
				return true;
			}
			if(player.getPosition()==13){
				player.setThePosition(2,19);
				return true;
			}
			if(player.getPosition()==14){
				player.setThePosition(7,2);
				return true;
			}
			if(player.getPosition()==17){
				player.setThePosition(15,2);
				return true;
			}
			if(player.getPosition()==16){
				player.setThePosition(10,18);
				return true;
			}
			if(player.getPosition()==18){
				player.setThePosition(19,2);
				return true;
			}
			if(player.getPosition()==19){
				player.setThePosition(18,10);
				return true;
			}
			if(player.getPosition()==20){
				player.setThePosition(19,19);
				return true;
			}
		}
		return false;
	}
	
	
}
