package model;

import java.util.ArrayList; 
import java.util.Collections; 
/**
 * This class is defined for the dice in the game.
 * @author chenhaoxian
 * @author Isabel Zhang
 * @author Meiyu Chen
 *
 */
public class Dice {
/**
 * ArrayList of integer was created to hold the value of the first dice.	
 */
	private ArrayList<Integer> _numbers1;
/**
 * ArrayList of integer was created to hold the value of the second dice.
 */
	private ArrayList<Integer> _numbers2;
/**
 * The constructor of the dice is to assign six numbers from 1 to 6 into two
 * arraylists which is similar to two dices.
 */
	public Dice(){
		Integer one = new Integer(1);
		Integer two = new Integer(2);
		Integer three = new Integer(3);
		Integer four = new Integer(4);
		Integer five = new Integer(5);
		Integer six = new Integer(6);
		
		_numbers1 = new ArrayList<Integer>();
		_numbers1.add(0, one);
		_numbers1.add(1, two);
		_numbers1.add(2, three);
		_numbers1.add(3, four);
		_numbers1.add(4, five);
		_numbers1.add(5, six);
		
	
		_numbers2 = new ArrayList<Integer>();
		_numbers2.add(0, one);
		_numbers2.add(1, two);
		_numbers2.add(2, three);
		_numbers2.add(3, four);
		_numbers2.add(4, five);
		_numbers2.add(5, six);
	}
/**
 * This method is created to get random values which is similar to throwing a 
 * dice.
 * @return
 */
	public int shuffle(){
		
	Collections.shuffle(_numbers1);
	Integer a = new Integer(0);
	a = _numbers1.get(0);
	Collections.shuffle(_numbers2);
	Integer b = new Integer(0);
	b = _numbers2.get(0);
	int total = a+b;
	//System.out.println(total);
	return total;
	}
}