package model;

import java.util.ArrayList;
import java.util.Collections;
/**
 * This class is to define cards in the game.
 * 
 * @author chenhaoxian
 * @author Isabel Zhang
 * @author Meiyu Chen
 */
public class Cards {
/**
 * 	ArrayList of Card is to hold the cards of characters.
 */
	private	ArrayList<Card> _SuspectCards;
/**
 * 	ArrayList of Card is to hold the cards of the weapons.
 */
	private ArrayList<Card> _WeaponsCards;
/**
 *  ArrayList of Card is to hold the cards of the rooms.
 */
	private ArrayList<Card> _RoomCards;
/**
 *  ArrayList of Card is to hold the rest of the cards after setting the envelope
 */
	private ArrayList<Card> _restOfTotal;
/**
 *  ArrayList of Card is to hold the cards in the envelope.
 */
	private ArrayList<Card> _answer;
/**
 * 	ArrayList of Card is to hold the cards suggested by the player.
 */
	private ArrayList<Card> _suggestion;
	
	private ArrayList<Card> _accusation;
/**
 *  Constructor of Cards define all the cards in the game.	
 */
	public Cards() {
		
		_suggestion = new ArrayList<Card>();
		_suggestion.add(0,null);
		_suggestion.add(1,null);
		_suggestion.add(2,null);
		_accusation = new ArrayList<Card>();
		_accusation.add(0,null);
		_accusation.add(1,null);
		_accusation.add(2,null);
		
		Card ColonelMustard = new Card("ColonelMustard");
		Card MissScarlet = new Card("MissScarlet");
		Card ProfessorPlum = new Card("ProfPlum");
		Card MrGreen = new Card("MrGreen");
		Card MrsWhite = new Card("MrsWhite");
		Card MrPeacock = new Card("MrsPeacock");

		Card PoisonedDonut = new Card("Candlestick");
		Card ExtendOGlove = new Card("Knife");
		Card Necklace = new Card("LeadPipe");
		Card Slingshot = new Card("Revolver");
		Card PlutoniumMod = new Card("Rope");
		Card Wrench = new Card("Wrench");

		Card study = new Card("Study");
		Card hall = new Card("Hall");
		Card lounge = new Card("Lounge");
		Card library = new Card("Library");
		Card billiardRoom = new Card("BillardRoom");
		Card diningRoom = new Card("DiningRoom");
		Card conservatory = new Card("Conservatory");
		Card ballRoom = new Card("BallRoom");
		Card kitchen = new Card("Kitchen");

		_SuspectCards = new ArrayList<Card>();
		_SuspectCards.add(ColonelMustard);
		_SuspectCards.add(MissScarlet);
		_SuspectCards.add(ProfessorPlum);
		_SuspectCards.add(MrGreen);
		_SuspectCards.add(MrsWhite);
		_SuspectCards.add(MrPeacock);

		_WeaponsCards = new ArrayList<Card>();
		_WeaponsCards.add(PoisonedDonut);
		_WeaponsCards.add(ExtendOGlove);
		_WeaponsCards.add(Necklace);
		_WeaponsCards.add(Slingshot);
		_WeaponsCards.add(PlutoniumMod);
		_WeaponsCards.add(Wrench);

		_RoomCards = new ArrayList<Card>();
		_RoomCards.add(study);
		_RoomCards.add(hall);
		_RoomCards.add(lounge);
		_RoomCards.add(library);
		_RoomCards.add(billiardRoom);
		_RoomCards.add(diningRoom);
		_RoomCards.add(conservatory);
		_RoomCards.add(ballRoom);
		_RoomCards.add(kitchen);
		
		_answer = new ArrayList<Card>();
		_restOfTotal = new ArrayList<Card>();
	
	}
/**
 * Take the top card from each group and place it in the envelope. This
 * should be done
 * carefully so that no player knows any of the three cards (one room,
 * one weapon, and
 * one suspect) placed in the envelope.
 * Shuffle together the three piles of remaining cards.
 * 	
 */
	
	
	public void setTheEnvelope(){
		
		Collections.shuffle(_SuspectCards);
		Collections.shuffle(_WeaponsCards);
		Collections.shuffle(_RoomCards);
		_answer.add(_SuspectCards.remove(0));
		_answer.add(_WeaponsCards.remove(0));
		_answer.add(_RoomCards.remove(0));
		
	}
/**
 * This method is used to return the answer of the game.
 * @return
 */
	public ArrayList<Card> getAnswer(){
		return _answer;
	}
/**
 * This method is defined to remove the first card in the three categories of cards,
 * and then add all the cards to one arraylist and then shuffle. So we could shuffle 
 * the rest cards.
 * @return
 */
	public ArrayList<Card> shuffleTheRest(){
		this.setTheEnvelope();
		_restOfTotal.addAll(_RoomCards);
		_restOfTotal.addAll(_WeaponsCards);
		_restOfTotal.addAll(_SuspectCards);
		Collections.shuffle(_restOfTotal);
		return _restOfTotal;
	}
/**
 * This method is defined to split the cards into three players. Whenever one card
 * is given, i should increase by one until 18 which means that all the cards have
 * been split.
 * 	
 * @param one
 * @param two
 * @param three
 */
	public void getEachPile3(Player one,Player two,Player three){
		ArrayList<Card> restTotal = new ArrayList<Card>();
		restTotal = shuffleTheRest(); 
		
		ArrayList<Card> cardsOfOne = one.getMyCards();
		ArrayList<Card> cardsOfTwo = two.getMyCards();
		ArrayList<Card> cardsOfThree = three.getMyCards();
		
		for(int i=0;i<18;i++){
			cardsOfOne.add(restTotal.get(i));
			i++;
			if(i<18){
			cardsOfTwo.add(restTotal.get(i));
			i++;
			}
			if(1<18){
			cardsOfThree.add(restTotal.get(i));
			}
		}
		one.setMyCards(cardsOfOne);
	    //System.out.println(cardsOfOne);
		two.setMyCards(cardsOfTwo);
		//System.out.println(cardsOfTwo);
		three.setMyCards(cardsOfThree);
		//System.out.println(cardsOfThree);
	}
/**
 * This method is defined to split the cards into four players. Whenever one card
 * is given, i should increase by one until 18 which means that all the cards have
 * been split.
 * @param one
 * @param two
 * @param three
 * @param four
 */
	public void getEachPile4(Player one,Player two,Player three,Player four){
		ArrayList<Card> restTotal = new ArrayList<Card>();
		restTotal = shuffleTheRest(); 
		
		ArrayList<Card> cardsOfOne = one.getMyCards();
		ArrayList<Card> cardsOfTwo = two.getMyCards();
		ArrayList<Card> cardsOfThree = three.getMyCards();
		ArrayList<Card> cardsOfFour = four.getMyCards();
		
		for(int i=0;i<18;i++){
			cardsOfOne.add(restTotal.get(i));
			i++;
			if(i<18){
			cardsOfTwo.add(restTotal.get(i));
			i++;
			}
			if(i<18){
			cardsOfThree.add(restTotal.get(i));	
			i++;
			}
			if(i<18){
			cardsOfFour.add(restTotal.get(i));
			}
		}
		one.setMyCards(cardsOfOne);
		two.setMyCards(cardsOfTwo);
		three.setMyCards(cardsOfThree);
		four.setMyCards(cardsOfFour);
	}
/**
 * This method is defined to split the cards into five players. Whenever one card
 * is given, i should increase by one until 18 which means that all the cards have
 * been split.
 * @param one
 * @param two
 * @param three
 * @param four
 * @param five
 */
	public void getEachPile5(Player one,Player two,Player three,Player four,Player five){
		ArrayList<Card> restTotal = new ArrayList<Card>();
		restTotal = shuffleTheRest(); 
		
		ArrayList<Card> cardsOfOne = one.getMyCards();
		ArrayList<Card> cardsOfTwo = two.getMyCards();
		ArrayList<Card> cardsOfThree = three.getMyCards();
		ArrayList<Card> cardsOfFour = four.getMyCards();
		ArrayList<Card> cardsOfFive = five.getMyCards();
		
		for(int i=0;i<18;i++){
			cardsOfOne.add(restTotal.get(i));
			i++;
			if(i<18){
			cardsOfTwo.add(restTotal.get(i));
			i++;
			}
			if(i<18){
			cardsOfThree.add(restTotal.get(i));
			i++;
			}
			if(i<18){
			cardsOfFour.add(restTotal.get(i));
			i++;
			}
			if(i<18){
			cardsOfFive.add(restTotal.get(i));
			}
		}
		one.setMyCards(cardsOfOne);
		two.setMyCards(cardsOfTwo);
		three.setMyCards(cardsOfThree);
		four.setMyCards(cardsOfFour);
		five.setMyCards(cardsOfFive);
	}
/**
 * This method is defined to split the cards into six players. Whenever one card
 * is given, i should increase by one until 18 which means that all the cards have
 * been split.
 * @param one
 * @param two
 * @param three
 * @param four
 * @param five
 * @param six
 */
	public void getEachPile6(Player one,Player two,Player three,Player four,Player five,Player six){
		ArrayList<Card> restTotal = new ArrayList<Card>();
		restTotal = shuffleTheRest(); 
		
		ArrayList<Card> cardsOfOne = one.getMyCards();
		ArrayList<Card> cardsOfTwo = two.getMyCards();
		ArrayList<Card> cardsOfThree = three.getMyCards();
		ArrayList<Card> cardsOfFour = four.getMyCards();
		ArrayList<Card> cardsOfFive = five.getMyCards();
		ArrayList<Card> cardsOfSix = six.getMyCards();
		
		for(int i=0;i<18;i++){
			cardsOfOne.add(restTotal.get(i));
			i++;
			if(i<18){
			cardsOfTwo.add(restTotal.get(i));
			i++;
			}
			if(i<18){
			cardsOfThree.add(restTotal.get(i));
			i++;
			}
			if(i<18){
			cardsOfFour.add(restTotal.get(i));
			i++;
			}
			if(i<18){
			cardsOfFive.add(restTotal.get(i));
			i++;
			}
			if(i<18){
			cardsOfSix.add(restTotal.get(i));
			}
		}
		one.setMyCards(cardsOfOne);
		two.setMyCards(cardsOfTwo);
		three.setMyCards(cardsOfThree);
		four.setMyCards(cardsOfFour);
		five.setMyCards(cardsOfFive);
		six.setMyCards(cardsOfSix);
	}
/** 
 * This method is created to hold the cards suggested by the player and other players
 * still have the right to see these cards.
 * @return
 */
	public ArrayList<Card> getSuggestion(){
		
		return _suggestion;
		
	}
	
	public ArrayList<Card> getAccusation(){
		
		return _accusation;
		
	}
	
	public void clearSuggestion(){
		_suggestion.clear();
	}
	
	public void clearAccusation(){
		_accusation.clear();
	}
	
	public boolean checkAns(){
		
		for(int i=0; i<_suggestion.size(); ++i){
			boolean out = false;
			for(int j=0; j<_answer.size(); ++j)
				if(_suggestion.get(i).getString().equals(_answer.get(j).getString()))
					out = true;
			if(!out){return false;}
		}	
		return true;
	}
	
	public boolean checkAnsFinal(){
		
		for(int i=0; i<_accusation.size(); ++i){
			boolean out = false;
			for(int j=0; j<_answer.size(); ++j)
				if(_accusation.get(i).getString().equals(_answer.get(j).getString()))
					out = true;
			if(!out){return false;}
		}	
		return true;
	}
	
}
