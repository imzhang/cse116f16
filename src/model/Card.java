package model;

/**
 * This class is to define the card element in the game.
 * 
 * @author chenhaoxian
 * @author Isabel Zhang
 * @author Meiyu Chen
 *
 */
public class Card {
/**
 * String is created to represent the content in the card.
 */
	private String _label;
/**
 * In the constructor, we associate the string with the Card meaning that each
 * card has its content.
 * @param name
 */
	public Card(String name) {

		_label = name;
	}
	
	public String getString(){
		return _label;
	}
}

