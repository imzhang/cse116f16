package Listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import GUI.GUI;

public class CListener implements ActionListener{
	int i;
	String[] suggest;
	String[] charNames;
	GUI gui;
	public CListener(String[] s, String[] w ,int in, GUI g){
		i = in;
		suggest = s;
		charNames = w;
		gui = g;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(suggest[1] == "")
			suggest[1] = charNames[i];
		if(suggest[0] != "")
			gui.updateSuggest2();
	}
	
}
