package Listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import GUI.GUI;
import model.Cards;

public class ARListener implements ActionListener{
	int i;
	String[] suggest;
	String[] roomNames;
	GUI gui;
	Cards _cards;
	
	public ARListener(String[] s, String[] w ,int in, GUI g, Cards cards){
		i = in;
		suggest = s;
		roomNames = w;
		gui = g;
		_cards = cards;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(suggest[2] == "")
			suggest[2] = roomNames[i];
		if(suggest[0] != ""&&suggest[1] != ""){
			gui.updateAccusation2();
			
			if(_cards.checkAnsFinal()){
				gui.restartButton();
				
			}else{
				gui.endAccu();
				String n="Oh, you got a wrong answer!";
				JOptionPane.showMessageDialog(gui,n);
			}
		}
	
	}
}
