package Listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import GUI.GUI;
import GUI.Restart;
import model.Card;
import model.Cards;

public class NextListener implements ActionListener{
	Cards cards;
	GUI gui;
	int block;
	int suggester;
	int curPlayer;
	
	public NextListener(Cards c, GUI g, int b,int s, int cur){
		cards = c;
		block = b;
		gui = g;
		suggester = s;
		curPlayer = cur;
	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		if( curPlayer == suggester && cards.checkAns()){
			System.out.println("hahaha");
			gui.restartButton();
		}
		else{
			gui.updateReject();
			gui.updateScreen();
			gui.updateNext();
			gui.updateCards();
		}
	}


}
