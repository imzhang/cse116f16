package Listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import GUI.GUI;
import model.Cards;

public class AWListener implements ActionListener{
	int i;
	String[] suggest;
	String[] weaponNames;
	GUI gui;
	Cards _cards;
	
	public AWListener(String[] s, String[] w ,int in, GUI g, Cards cards){
		i = in;
		suggest = s;
		weaponNames = w;
		gui = g;
		_cards = cards;
		
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(suggest[0] == "")
			suggest[0] = weaponNames[i];
		if(suggest[1] != ""&&suggest[2] != ""){
			gui.updateAccusation2();
			
			if(_cards.checkAnsFinal()){
				gui.restartButton();
			
			}else{
				gui.endAccu();
				String n="Oh, you got a wrong answer!";
				JOptionPane.showMessageDialog(gui,n);
			}
		}
	}
	
}
