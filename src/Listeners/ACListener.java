package Listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import GUI.GUI;
import model.Cards;
import model.Player;

public class ACListener implements ActionListener{
	int i;
	String[] suggest;
	String[] charNames;
	GUI gui;
	Cards _cards;

	
	public ACListener(String[] s, String[] w ,int in, GUI g, Cards cards){
		i = in;
		suggest = s;
		charNames = w;
		gui = g;
		_cards = cards;
		
		
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(suggest[1] == "")
			suggest[1] = charNames[i];
		if(suggest[0] != ""&&suggest[2] != ""){
			gui.updateAccusation2();
			
			if(_cards.checkAnsFinal()){
				gui.restartButton();
				
			}else{
				gui.endAccu();
				 String n="Oh, you got a wrong answer!";
           	  JOptionPane.showMessageDialog(gui,n);
			}
		}
		
	}	
	
}
