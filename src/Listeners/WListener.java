package Listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import GUI.GUI;

public class WListener implements ActionListener{
	int i;
	String[] suggest;
	String[] weaponNames;
	GUI gui;
	public WListener(String[] s, String[] w ,int in, GUI g){
		i = in;
		suggest = s;
		weaponNames = w;
		gui = g;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(suggest[0] == "")
			suggest[0] = weaponNames[i];
		if(suggest[1] != "")
			gui.updateSuggest2();
	}
	
}
